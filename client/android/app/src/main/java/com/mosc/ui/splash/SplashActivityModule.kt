package com.mosc.ui.splash

import com.mosc.ui.splash.interactor.SplashInteractor
import com.mosc.ui.splash.interactor.SplashMVPInteractor
import com.mosc.ui.splash.presenter.SplashMVPPresenter
import com.mosc.ui.splash.presenter.SplashPresenter
import com.mosc.ui.splash.view.SplashMVPView
import dagger.Module
import dagger.Provides

@Module
class SplashActivityModule {

    @Provides
    internal fun provideSplashInteractor(splashInteractor: SplashInteractor): SplashMVPInteractor = splashInteractor

    @Provides
    internal fun provideSplashPresenter(splashPresenter: SplashPresenter<SplashMVPView, SplashMVPInteractor>)
            : SplashMVPPresenter<SplashMVPView, SplashMVPInteractor> = splashPresenter
}