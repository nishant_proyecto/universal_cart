package com.mosc.ui.base.view

import android.support.annotation.StringRes



interface MVPView {

    fun showProgress()

    fun hideProgress()

    fun onError(@StringRes resId: Int)

    fun onError(message: String)

    fun showMessage(message: String)

    fun showMessage(@StringRes resId: Int)

    fun isNetworkConnected(): Boolean

}