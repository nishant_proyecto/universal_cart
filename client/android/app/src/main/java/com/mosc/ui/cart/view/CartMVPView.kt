package com.mosc.ui.cart.view

import com.mosc.data.network.FavouriteResponse
import com.mosc.ui.base.view.MVPView

interface CartMVPView : MVPView {

    fun getSearchResultList(result : List<FavouriteResponse>)

    fun deleteSuccess(id : String);
}