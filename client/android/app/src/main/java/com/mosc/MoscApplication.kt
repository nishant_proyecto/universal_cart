package com.mosc

import android.app.Activity
import android.app.Application
import android.provider.Settings
import com.chibatching.kotpref.Kotpref
import com.mosc.di.component.DaggerAppComponent
import com.mosc.utils.DeviceUtils
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import timber.log.Timber
import javax.inject.Inject

class MoscApplication : Application(), HasActivityInjector{

    @Inject
    lateinit internal var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>


    override fun activityInjector() = activityDispatchingAndroidInjector

    override fun onCreate() {
        super.onCreate()

        Timber.tag("cart")
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        Kotpref.init(this)

        DeviceUtils.userId = Settings.Secure.getString(this
                .getContentResolver(),
                Settings.Secure.ANDROID_ID);

        DaggerAppComponent.builder()
                .application(this)
                .build()
                .inject(this)
    }

}