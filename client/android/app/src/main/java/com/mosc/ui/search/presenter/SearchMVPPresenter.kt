package com.mosc.ui.search.presenter

import com.mosc.data.network.FavouriteRequest
import com.mosc.data.network.SearchResponse
import com.mosc.ui.base.presenter.MVPPresenter
import com.mosc.ui.search.interactor.SearchMVPInteractor
import com.mosc.ui.search.view.SearchMVPView
import io.reactivex.Observable
import org.json.JSONObject

interface SearchMVPPresenter <V : SearchMVPView, I : SearchMVPInteractor> : MVPPresenter<V, I>{

    fun fetchSearchResult(item : String)

    fun addFavItem(detail : FavouriteRequest)

    fun addUser(id: String?)
}