package com.mosc.data.network

data class SearchResponse ( var id: String? = null,
                            var img: String? = null,
                            var description: String? = null,
                            var width: String? = null,
                            var height: String? = null,
                            var src: String? = null,
                            var href: String? = null,
                            var quantity: Int? = null,
                            var price: String? = null)