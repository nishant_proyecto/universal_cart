package com.mosc.data.network

import io.reactivex.Observable
import org.json.JSONObject
import retrofit2.http.*

interface ApiEndpoint {

    @GET("/search/{keyword}")
    fun getPosts(@Path("keyword") q: String?): Observable<List<SearchResponse>>

    @POST("/start_tracker")
    fun sendFavoriteItem(@Body itemDetails: FavouriteRequest) : Observable<JSONObject>

    @GET("/product_user/{user_id}")
    fun getFavouritrItems(@Path("user_id") q: String?) : Observable<List<FavouriteResponse>>

    @POST("/user")
    @FormUrlEncoded
    fun addUser(@Field("id") id: String?,
                @Field("gcm_id") gcm_id: String?) : Observable<String>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "/product_user", hasBody = true)
    fun deleteProduct(@Field("userId") userId: String?,
                @Field("productId") productId: String?) : Observable<String>
}