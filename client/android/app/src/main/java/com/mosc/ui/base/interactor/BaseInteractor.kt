package com.mosc.ui.base.interactor

import com.mosc.data.network.ApiHelper


open class BaseInteractor() : MVPInteractor {

    protected lateinit var apiHelper: ApiHelper

    constructor(apiHelper: ApiHelper) : this() {
        this.apiHelper = apiHelper
    }


}