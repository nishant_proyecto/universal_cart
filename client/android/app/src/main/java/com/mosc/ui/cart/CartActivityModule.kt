package com.mosc.ui.cart

import com.mosc.ui.cart.interactor.CartInteractor
import com.mosc.ui.cart.interactor.CartMVPInteractor
import com.mosc.ui.cart.presenter.CartMVPPresenter
import com.mosc.ui.cart.presenter.CartPresenter
import com.mosc.ui.cart.view.CartMVPView
import dagger.Module
import dagger.Provides

@Module
class CartActivityModule {

    @Provides
    internal fun provideCartInteractor(cartInteractor: CartInteractor): CartMVPInteractor = cartInteractor

    @Provides
    internal fun provideCartPresenter(cartPresenter: CartPresenter<CartMVPView, CartMVPInteractor>)
            : CartMVPPresenter<CartMVPView, CartMVPInteractor> = cartPresenter

}