package com.mosc.data.network

import io.reactivex.Observable
import org.json.JSONObject
import javax.inject.Inject

class AppApiHelper @Inject constructor(private val apiEndpoint : ApiEndpoint): ApiHelper  {

    override fun deleteProduct(userId: String?, productId: String?): Observable<String>
            = apiEndpoint.deleteProduct(userId, productId)

    override fun addUser(id: String?, gcm_id: String?): Observable<String> = apiEndpoint.addUser(id, gcm_id)

    override fun getFavouriteResult(item: String): Observable<List<FavouriteResponse>>
            = apiEndpoint.getFavouritrItems(item)

    override fun sendFavItem(detail: FavouriteRequest): Observable<JSONObject> = apiEndpoint.sendFavoriteItem(detail)


    override fun getSearchResult(item : String): Observable<List<SearchResponse>> =  apiEndpoint.getPosts(item)

}