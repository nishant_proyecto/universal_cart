package com.mosc.ui.search

import com.mosc.ui.search.interactor.SearchInteractor
import com.mosc.ui.search.interactor.SearchMVPInteractor
import com.mosc.ui.search.presenter.SearchMVPPresenter
import com.mosc.ui.search.presenter.SearchPresenter
import com.mosc.ui.search.view.SearchMVPView
import dagger.Module
import dagger.Provides

@Module
class SearchActivityModule {

    @Provides
    internal fun provideSearchInteractor(searchInteractor: SearchInteractor): SearchMVPInteractor = searchInteractor

    @Provides
    internal fun provideSearchPresenter(searchPresenter: SearchPresenter<SearchMVPView, SearchMVPInteractor>)
            : SearchMVPPresenter<SearchMVPView, SearchMVPInteractor> = searchPresenter

}