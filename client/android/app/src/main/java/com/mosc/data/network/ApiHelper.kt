package com.mosc.data.network

import io.reactivex.Observable
import org.json.JSONObject

interface ApiHelper {

    fun getSearchResult(item : String) : Observable<List<SearchResponse>>

    fun getFavouriteResult(item : String) : Observable<List<FavouriteResponse>>

    fun sendFavItem(detail : FavouriteRequest) : Observable<JSONObject>

    fun addUser(id: String?, gcm_id: String?) : Observable<String>

    fun deleteProduct(userId: String?, productId: String?) : Observable<String>
}