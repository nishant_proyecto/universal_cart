package com.mosc.ui.cart.presenter

import com.mosc.ui.base.presenter.MVPPresenter
import com.mosc.ui.cart.interactor.CartMVPInteractor
import com.mosc.ui.cart.view.CartMVPView


interface CartMVPPresenter <V : CartMVPView, I : CartMVPInteractor> : MVPPresenter<V, I> {

    fun fetchFavResult(userId : String)

    fun delete(productId : String)

}