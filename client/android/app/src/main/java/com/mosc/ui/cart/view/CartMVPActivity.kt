package com.mosc.ui.cart.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.mosc.R
import com.mosc.data.network.FavouriteResponse
import com.mosc.ui.base.view.BaseActivity
import com.mosc.ui.cart.interactor.CartMVPInteractor
import com.mosc.ui.cart.presenter.CartMVPPresenter
import com.mosc.utils.DeviceUtils
import kotlinx.android.synthetic.main.activity_cart.*
import javax.inject.Inject


class CartMVPActivity : BaseActivity(), CartMVPView {

    override fun deleteSuccess(id : String) {
        val favList  = favouriteList.filter { favouriteResponse -> favouriteResponse.id != id }


        runOnUiThread {
            getSearchResultList(favList)
        }

    }


    override fun getSearchResultList(result: List<FavouriteResponse>) {
        if (favouriteList.size>0) {
            favouriteList.clear()
            rv_fav_result.adapter.notifyDataSetChanged()
        }

        favouriteList.addAll(result)

        rv_fav_result.adapter.notifyItemInserted(favouriteList.size)
        loadingCompleted()
    }

    @Inject
    lateinit var presenter: CartMVPPresenter<CartMVPView, CartMVPInteractor>

    private var favouriteList: ArrayList<FavouriteResponse> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)

        presenter.onAttach(this)

        // Creates a vertical Layout Manager
        rv_fav_result.layoutManager = LinearLayoutManager(this)

        val horizontalDecoration = DividerItemDecoration(rv_fav_result.context,
                DividerItemDecoration.VERTICAL)
        val horizontalDivider = ContextCompat.getDrawable(this, R.drawable.drawable_divider)
        horizontalDecoration.setDrawable(horizontalDivider!!)

        rv_fav_result.addItemDecoration(horizontalDecoration)


        rv_fav_result.adapter = FavouriteListAdapter(favouriteList,
                this, onClickListener = {  view, item -> handleClick(view, item)})

        isLoading()
        presenter.fetchFavResult(DeviceUtils.userId)
    }

    private fun handleClick(view: View, item: FavouriteResponse) {

        if (view is AppCompatImageView){
            presenter.delete(item.id.toString())
        }else{
            var browserIntent : Intent

            val url = item.href
            browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(browserIntent)
        }
    }

    fun isLoading(){
        fav_pb_loading.visibility = View.VISIBLE
        rv_fav_result.visibility = View.GONE
        tv_empty_cart.visibility = View.GONE
    }

    fun loadingCompleted(){
        fav_pb_loading.visibility = View.GONE


        if (favouriteList.size==0 || favouriteList.size<0){
            tv_empty_cart.visibility = View.VISIBLE
            rv_fav_result.visibility = View.GONE
        }else{
            tv_empty_cart.visibility = View.GONE
            rv_fav_result.visibility = View.VISIBLE
        }
    }

    override fun onFragmentAttached() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFragmentDetached(tag: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}