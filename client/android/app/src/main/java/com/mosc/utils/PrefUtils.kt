package com.mosc.utils

import com.chibatching.kotpref.KotprefModel

object PrefUtils : KotprefModel() {
    var token by stringPref()
}