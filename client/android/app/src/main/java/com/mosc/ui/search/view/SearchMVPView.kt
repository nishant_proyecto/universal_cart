package com.mosc.ui.search.view

import com.mosc.data.network.SearchResponse
import com.mosc.ui.base.view.MVPView
import org.json.JSONObject

interface SearchMVPView : MVPView {

    fun getSearchResultList(result : List<SearchResponse>)

    fun itemAdded(result : JSONObject)

}