package com.mosc.ui.search.interactor

import com.mosc.data.network.FavouriteRequest
import com.mosc.data.network.SearchResponse
import com.mosc.ui.base.interactor.MVPInteractor
import io.reactivex.Observable
import org.json.JSONObject

interface SearchMVPInteractor : MVPInteractor{

    fun getSearchReasult(item : String) : Observable<List<SearchResponse>>

    fun sendFavItem(detail : FavouriteRequest) : Observable<JSONObject>

    fun addUser(id: String?) : Observable<String>

}