package com.mosc.ui.search.view

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton
import com.mosc.R
import com.mosc.data.network.SearchResponse
import kotlinx.android.synthetic.main.row_details.view.*
import kotlinx.android.synthetic.main.row_search_result.view.*
import timber.log.Timber

class SearchListAdapter(var items : List<SearchResponse>,
                        val context: Context,
                        val onClickListener: (View, SearchResponse) -> Unit) : RecyclerView.Adapter<SearchListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_search_result, parent, false))
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.tvItemDescription?.text = items.get(position).description
        holder?.tv_item_cost?.text = context.getString(R.string.dollar_value, items.get(position).price)
        holder?.tv_total?.text = context.getString(R.string.total_30, items.get(position).price)
        holder?.tv_item_product_number_value?.text = items.get(position).id
        items.get(position).quantity = holder?.nb_quantity.number.toInt()

        Glide.with(context)
                .load(items.get(position).src)
                .into(holder?.iv_item_pic)

        holder?.btn_add_fav.setOnClickListener{

            onClickListener.invoke(it, items.get(position))
        }

        holder?.nb_quantity.setOnValueChangeListener(ElegantNumberButton.OnValueChangeListener {
            view, oldValue, newValue ->
            items.get(position).quantity = newValue

            var total : Float = items.get(position).price?.replace("$", "")?.toFloat()?.times(newValue)
                    ?: 0f;
            holder?.tv_total?.text = context.getString(R.string.total_30, total.toString())
        })
    }

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val tvItemDescription = view.tv_item_description
        val iv_item_pic = view.iv_item_pic
        val tv_item_cost = view.tv_item_cost
        val btn_add_fav = view.btn_add_fav
        val tv_item_product_number_value = view.tv_item_product_number_value
        val nb_quantity = view.nb_quantity
        val tv_total = view.tv_total
    }
}