package com.mosc.ui.cart.interactor

import android.content.Context
import com.mosc.data.network.ApiHelper
import com.mosc.data.network.FavouriteResponse
import com.mosc.data.network.SearchResponse
import com.mosc.ui.base.interactor.BaseInteractor
import com.mosc.utils.DeviceUtils
import com.mosc.utils.PrefUtils
import io.reactivex.Observable
import org.json.JSONObject
import javax.inject.Inject

class CartInteractor @Inject constructor(private val mContext : Context,
                                         apiHelper: ApiHelper) : BaseInteractor(apiHelper),CartMVPInteractor{
    override fun deleteProduct(productId: String): Observable<String> {

        return apiHelper.deleteProduct(DeviceUtils.userId, productId)
    }

    override fun getFavouriteItem(userId: String): Observable<List<FavouriteResponse>> {
        return apiHelper.getFavouriteResult(userId)
    }

}