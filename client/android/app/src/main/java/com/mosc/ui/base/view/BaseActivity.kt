package com.mosc.ui.base.view

import android.app.ProgressDialog
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.mosc.utils.CommonUtil
import dagger.android.AndroidInjection


abstract class BaseActivity : AppCompatActivity(), MVPView, BaseFragment.CallBack {

     override fun showMessage(message: String) {
         if (message != null) {
             Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
         } else {
             Toast.makeText(this, "Unknown Error", Toast.LENGTH_SHORT).show();
         }
     }

    override fun onError(@StringRes resId: Int){

    }

    override fun onError(message: String){
        showMessage(message)
    }

     override fun showMessage(resId: Int) {
         showMessage(getString(resId));
     }

     override fun isNetworkConnected(): Boolean {
         TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
     }

     private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        performDI()
    }

    override fun hideProgress() {
        progressDialog?.let { if (it.isShowing) it.cancel() }
    }

    override fun showProgress() {
        hideProgress()
        progressDialog = CommonUtil.showLoadingDialog(this)
    }

    private fun performDI() = AndroidInjection.inject(this)

}