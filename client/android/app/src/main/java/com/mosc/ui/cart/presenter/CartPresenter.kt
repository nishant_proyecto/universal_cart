package com.mosc.ui.cart.presenter

import com.mosc.ui.base.presenter.BasePresenter
import com.mosc.ui.cart.interactor.CartMVPInteractor
import com.mosc.ui.cart.view.CartMVPView
import com.mosc.utils.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class CartPresenter  <V : CartMVPView, I : CartMVPInteractor>
@Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider,
                             disposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor,
        schedulerProvider = schedulerProvider, compositeDisposable = disposable), CartMVPPresenter<V, I> {

    override fun delete(productId: String) {
        getView()?.showProgress()
        interactor?.let {
            compositeDisposable.add(it.deleteProduct(productId)
                .compose(schedulerProvider.ioToMainObservableScheduler())
                .subscribe({
                    getView()?.hideProgress()
                    getView()?.deleteSuccess(productId)
                }, {
                    getView()?.hideProgress()
                    getView()?.onError(it.localizedMessage)
                }))
        }
    }


    override fun fetchFavResult(userId: String) {
        interactor?.let{
            compositeDisposable.add(it.getFavouriteItem(userId)
                .compose(schedulerProvider.ioToMainObservableScheduler())
                .subscribe({
                    getView()?.getSearchResultList(it)
                }, {
                    getView()?.onError(it.localizedMessage)
                }))
        }
    }

}