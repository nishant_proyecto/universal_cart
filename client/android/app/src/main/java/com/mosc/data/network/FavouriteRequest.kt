package com.mosc.data.network

import com.mosc.utils.DeviceUtils
import com.mosc.utils.PrefUtils

data class FavouriteRequest (var id: String? = null,
                             var gcm_id: String? = PrefUtils.token,
                             var user_id: String? = DeviceUtils.userId,
                             var href: String? = null,
                             var src: String? = null,
                             var width: String? = null,
                             var height: String? = null,
                             var description: String? = null,
                             var price: String? = null,
                             var quantity: Int? = null,
                             var company: String? = "Amazon")
