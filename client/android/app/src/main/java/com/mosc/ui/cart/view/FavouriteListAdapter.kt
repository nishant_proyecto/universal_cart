package com.mosc.ui.cart.view

import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.mosc.R
import com.mosc.data.network.FavouriteResponse
import kotlinx.android.synthetic.main.row_favourite_details.view.*
import kotlinx.android.synthetic.main.row_favourite_result.view.*
import java.text.SimpleDateFormat
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class FavouriteListAdapter(var items : List<FavouriteResponse>,
                           val context: Context,
                           val onClickListener: (View, FavouriteResponse) -> Unit) :
                                    RecyclerView.Adapter<FavouriteListAdapter.ViewHolder>() {

    val pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

    object DateUtils {
        @JvmStatic
        fun toSimpleString(date: Date) : String {
            val format = SimpleDateFormat("dd MMM yyy")
            return format.format(date)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_favourite_result, parent, false))
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    fun toSimpleString(date: String?) : String{
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            var zonedDateTime = ZonedDateTime.parse(date)
            return DateTimeFormatter.ofPattern("dd MMM yyyy").format(zonedDateTime)
        } else {
            val date = SimpleDateFormat(pattern).parse(date)
            return DateUtils.toSimpleString(date)
        }
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var favObject : FavouriteResponse = items.get(position);

        holder?.tvItemDescription?.text = favObject.description
        if (favObject.price!=null && favObject.quantity!=null) {
            holder?.tv_item_cost?.text = context.getString(R.string.dollar_value,
                    (favObject.price?.toFloat()?.times(favObject.quantity!!)).toString())
        }else{
            holder?.tv_item_cost?.text = context.getString(R.string.dollar_value,
                    (favObject.price?.toFloat()).toString())
        }
        holder?.tv_date_text?.text = context.getString(R.string.add_date, toSimpleString(favObject.updatedAt))
        holder?.tv_item_product_number_value?.text = favObject.id

        if (favObject.quantity!=null)
            holder?.tv_quantity?.text = favObject.quantity.toString()
        else
            holder?.tv_quantity?.text = "1"

        if (favObject.updated_price != null &&
                favObject.updated_price!!.length>0) {
            holder?.tv_item_cost?.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            holder?.tv_item_cost?.setTextColor(Color.RED);
            holder?.tv_item_update_cost?.visibility = VISIBLE
            holder?.tv_item_update_cost?.text = context.getString(R.string.dollar_value,
                    (favObject.updated_price?.toFloat()?.times(favObject.quantity!!)).toString())
        }
        Glide.with(context)
                .load(favObject.src)
                .into(holder?.iv_item_pic)


        holder?.btn_add_fav.setOnClickListener{
            onClickListener.invoke(it, favObject)
        }

        holder?.ic_delete.setOnClickListener{
            onClickListener.invoke(it, favObject)
        }
    }

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val tvItemDescription = view.tv_item_description
        val iv_item_pic = view.iv_item_pic
        val tv_item_cost = view.tv_item_cost
        val tv_item_update_cost = view.tv_item_updated_cost
        val tv_quantity = view.tv_quantity
        val tv_date_text = view.tv_date_text
        val tv_item_product_number_value = view.tv_item_product_number_value

        val btn_add_fav = view.btn_add_fav
        val ic_delete = view.ic_delete

    }
}