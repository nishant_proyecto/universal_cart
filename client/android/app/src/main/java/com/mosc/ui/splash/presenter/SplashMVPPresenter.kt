package com.mosc.ui.splash.presenter

import com.mosc.ui.base.presenter.MVPPresenter
import com.mosc.ui.splash.interactor.SplashMVPInteractor
import com.mosc.ui.splash.view.SplashMVPView

interface SplashMVPPresenter<V : SplashMVPView, I : SplashMVPInteractor> : MVPPresenter<V, I>