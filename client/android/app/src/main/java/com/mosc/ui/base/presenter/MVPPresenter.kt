package com.mosc.ui.base.presenter

import com.mosc.ui.base.interactor.MVPInteractor
import com.mosc.ui.base.view.MVPView


interface MVPPresenter<V : MVPView, I : MVPInteractor> {

    fun onAttach(view: V?)

    fun onDetach()

    fun getView(): V?

}