package com.mosc.ui.search.interactor

import android.content.Context
import com.mosc.data.network.ApiEndpoint
import com.mosc.data.network.ApiHelper
import com.mosc.data.network.FavouriteRequest
import com.mosc.data.network.SearchResponse
import com.mosc.ui.base.interactor.BaseInteractor
import com.mosc.utils.PrefUtils
import io.reactivex.Observable
import org.json.JSONObject
import javax.inject.Inject

class SearchInteractor @Inject constructor(private val mContext : Context,
                                           apiHelper: ApiHelper) : BaseInteractor(apiHelper),
        SearchMVPInteractor {

    override fun addUser(id: String?): Observable<String> {
        return apiHelper.addUser(id, PrefUtils.token)
    }


    override fun sendFavItem(detail: FavouriteRequest): Observable<JSONObject> {
        return apiHelper.sendFavItem(detail)
    }


    override fun getSearchReasult(item: String) : Observable<List<SearchResponse>> {
        return apiHelper.getSearchResult(item)
    }


}