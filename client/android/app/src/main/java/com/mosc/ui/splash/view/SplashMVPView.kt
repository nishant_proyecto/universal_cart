package com.mosc.ui.splash.view

import com.mosc.ui.base.view.MVPView


interface SplashMVPView : MVPView {
    fun openSearchActivity()
}