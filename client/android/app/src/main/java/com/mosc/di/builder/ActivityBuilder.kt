package com.mosc.di.builder

import com.mosc.ui.cart.CartActivityModule
import com.mosc.ui.cart.view.CartMVPActivity
import com.mosc.ui.search.SearchActivityModule
import com.mosc.ui.search.view.SearchMVPActivity
import com.mosc.ui.splash.SplashActivityModule
import com.mosc.ui.splash.view.SplashMVPActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder{

    @ContributesAndroidInjector(modules = [(SplashActivityModule::class)])
    abstract fun bindSplashActivity(): SplashMVPActivity

    @ContributesAndroidInjector(modules = [(SearchActivityModule::class)])
    abstract fun bindSearchActivity(): SearchMVPActivity

    @ContributesAndroidInjector(modules = [(CartActivityModule::class)])
    abstract fun bindCartActivity(): CartMVPActivity
}