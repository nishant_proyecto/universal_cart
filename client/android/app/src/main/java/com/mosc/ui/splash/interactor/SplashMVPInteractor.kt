package com.mosc.ui.splash.interactor

import com.mosc.ui.base.interactor.MVPInteractor
import io.reactivex.Observable


interface SplashMVPInteractor : MVPInteractor {

    fun checkSplashTimeout() : Observable<Long>

}