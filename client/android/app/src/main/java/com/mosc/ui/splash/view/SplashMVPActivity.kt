package com.mosc.ui.splash.view

import android.content.Intent
import android.os.Bundle
import com.mosc.ui.base.view.BaseActivity
import com.mosc.ui.search.view.SearchMVPActivity
import com.mosc.ui.splash.interactor.SplashMVPInteractor
import com.mosc.ui.splash.presenter.SplashMVPPresenter
import javax.inject.Inject
import timber.log.Timber


class SplashMVPActivity : BaseActivity(), SplashMVPView {

    @Inject
    lateinit var presenter: SplashMVPPresenter<SplashMVPView, SplashMVPInteractor>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onAttach(this)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun onFragmentDetached(tag: String) {
    }

    override fun onFragmentAttached() {
    }


    override fun openSearchActivity() {
        val intent = Intent(this, SearchMVPActivity::class.java)
        startActivity(intent)
        finish()
    }

}
