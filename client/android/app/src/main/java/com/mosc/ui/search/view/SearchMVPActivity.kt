package com.mosc.ui.search.view

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.View
import android.widget.EditText
import com.google.gson.Gson
import com.mosc.R
import com.mosc.data.network.FavouriteRequest
import com.mosc.data.network.SearchResponse
import com.mosc.ui.base.view.BaseActivity
import com.mosc.ui.cart.view.CartMVPActivity
import com.mosc.ui.search.interactor.SearchMVPInteractor
import com.mosc.ui.search.presenter.SearchMVPPresenter
import com.mosc.utils.DeviceUtils
import com.mosc.utils.PrefUtils
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.row_details.view.*
import org.json.JSONObject
import timber.log.Timber
import javax.inject.Inject


class SearchMVPActivity : BaseActivity(), SearchMVPView{


    override fun itemAdded(result: JSONObject) {
        Timber.i(result.toString())
        showMessage("Item Added to your Universal Cart")
    }

    @Inject
    lateinit var presenter: SearchMVPPresenter<SearchMVPView, SearchMVPInteractor>

    private var searchList: ArrayList<SearchResponse> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_search)
        presenter.onAttach(this)

        app_title.setText(Html.fromHtml(getString(R.string.home_app_title)))

        val searchEditText = sv_search.findViewById(android.support.v7.appcompat.R.id.search_src_text) as EditText
        searchEditText.setTextColor(ContextCompat.getColor(this, android.R.color.black))
        searchEditText.setHintTextColor(ContextCompat.getColor(this, android.R.color.darker_gray))
        searchEditText.setHint(R.string.search_hint_text)
        sv_search.setIconified(false)

        // Creates a vertical Layout Manager
        rv_search_result.layoutManager = LinearLayoutManager(this)

        val horizontalDecoration = DividerItemDecoration(rv_search_result.context,
                DividerItemDecoration.VERTICAL)
        val horizontalDivider = ContextCompat.getDrawable(this, R.drawable.drawable_divider)
        horizontalDecoration.setDrawable(horizontalDivider!!)

        rv_search_result.addItemDecoration(horizontalDecoration)

        rv_search_result.adapter = SearchListAdapter(searchList, this, onClickListener = {  view, item -> handleClick(view, item)})

        btn_search.setOnClickListener{
            isLoading()
            if (sv_search.query.isNotEmpty())
                presenter.fetchSearchResult(sv_search.query.toString())
            else
            {
                searchList.clear()
                rv_search_result.adapter.notifyDataSetChanged()
                loadingCompleted()
            }
        }

        iv_universal_cart.setOnClickListener{
            val intent = Intent(this, CartMVPActivity::class.java)
            startActivity(intent)
        }


        // Add User to server
        presenter.addUser(DeviceUtils.userId)

    }

    private fun handleClick(view: View, item: SearchResponse) {

        var fav  = FavouriteRequest(id = item.id,
                gcm_id = PrefUtils.token,
                user_id = DeviceUtils.userId,
                href = item.href,
                src = item.src,
                width = item.width,
                height = item.height,
                description = item.description,
                quantity = item.quantity,
                price = item.price)

        presenter.addFavItem(fav)
    }

    fun isLoading(){
        pb_loading.visibility = View.VISIBLE
        rv_search_result.visibility = View.GONE
    }

    fun loadingCompleted(){
        pb_loading.visibility = View.GONE
        rv_search_result.visibility = View.VISIBLE
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun onFragmentDetached(tag: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getSearchResultList(result: List<SearchResponse>) {
        if (searchList.size>0) {
            searchList.clear()
            rv_search_result.adapter.notifyDataSetChanged()
        }

        searchList.addAll(result)
        rv_search_result.adapter.notifyItemInserted(searchList.size)
        loadingCompleted()
    }

    override fun onFragmentAttached() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}