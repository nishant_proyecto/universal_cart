package com.mosc.ui.search.presenter

import com.mosc.data.network.FavouriteRequest
import com.mosc.ui.base.presenter.BasePresenter
import com.mosc.ui.search.interactor.SearchMVPInteractor
import com.mosc.ui.search.view.SearchMVPView
import com.mosc.utils.SchedulerProvider
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import org.json.JSONObject
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class SearchPresenter <V : SearchMVPView, I : SearchMVPInteractor>
@Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider,
disposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor,
schedulerProvider = schedulerProvider, compositeDisposable = disposable), SearchMVPPresenter<V, I> {

    override fun addUser(id: String?){
        interactor?.let{
            compositeDisposable.add(it.addUser(id)
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe({
                        Timber.d("User added Successfully "+it)

                    }, {
                        Timber.e(it)
                        getView()?.onError(it.localizedMessage)
                    }))
        }
    }


    override fun addFavItem(detail: FavouriteRequest) {
        interactor?.let{
            compositeDisposable.add(it.sendFavItem(detail)
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe({
                        getView()?.itemAdded(it)
                    }, {
                        getView()?.onError(it.localizedMessage)
                    }))
        }
    }


    override fun fetchSearchResult(item : String) {

        interactor?.let {
            compositeDisposable.add(it.getSearchReasult(item)
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe({
                        getView()?.getSearchResultList(it)
                    }, {
                        getView()?.hideProgress()
                        getView()?.onError(it.localizedMessage)
                    }))

        }

    }


}