package com.mosc.ui.splash.presenter

import com.mosc.ui.base.presenter.BasePresenter
import com.mosc.ui.splash.interactor.SplashMVPInteractor
import com.mosc.ui.splash.view.SplashMVPView
import com.mosc.utils.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SplashPresenter<V : SplashMVPView, I : SplashMVPInteractor>
@Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider,
                             disposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor,
        schedulerProvider = schedulerProvider, compositeDisposable = disposable), SplashMVPPresenter<V, I> {

    override fun onAttach(view: V?) {
        super.onAttach(view)
        launchSearchActivity()
    }

    private fun launchSearchActivity()= interactor?.let {
        compositeDisposable.add(it.checkSplashTimeout()
                .compose(schedulerProvider.ioToMainObservableScheduler())
                .subscribe({
                    getView()?.let {
                        it.openSearchActivity()
                    }
                }))
    }

}