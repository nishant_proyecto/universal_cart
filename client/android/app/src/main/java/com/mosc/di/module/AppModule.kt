package com.mosc.di.module

import android.app.Application
import android.content.Context
import com.mosc.data.network.ApiEndpoint
import com.mosc.data.network.ApiHelper
import com.mosc.data.network.AppApiHelper
import com.mosc.utils.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule{

    // Free Server - http://universalcard-universalcart.1d35.starter-us-east-1.openshiftapps.com
    // Heroku - https://mosm.herokuapp.com

    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context = application

    @Provides
    internal fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    internal fun provideSchedulerProvider(): SchedulerProvider = SchedulerProvider()

    @Provides
    @Singleton
    internal fun provideApiHelper(appApiHelper: AppApiHelper): ApiHelper = appApiHelper

    @Provides
    internal fun provideApiEndpoint(retrofit: Retrofit): ApiEndpoint {
        return retrofit.create(ApiEndpoint::class.java)
    }

    @Provides
    internal fun provideRetrofitInterface(): Retrofit {
        return Retrofit.Builder()
                .baseUrl("https://mosm.herokuapp.com")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()
    }
}