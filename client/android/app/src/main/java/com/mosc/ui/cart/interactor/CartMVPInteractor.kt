package com.mosc.ui.cart.interactor

import com.mosc.data.network.FavouriteResponse
import com.mosc.data.network.SearchResponse
import com.mosc.ui.base.interactor.MVPInteractor
import io.reactivex.Observable
import org.json.JSONObject

interface CartMVPInteractor : MVPInteractor {

    fun getFavouriteItem(item : String) : Observable<List<FavouriteResponse>>


    fun deleteProduct(productId : String) : Observable<String>

}