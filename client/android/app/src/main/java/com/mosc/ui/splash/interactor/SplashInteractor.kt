package com.mosc.ui.splash.interactor

import android.content.Context
import com.mosc.data.network.ApiHelper
import com.mosc.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SplashInteractor @Inject constructor(private val mContext: Context,
                                           apiHelper: ApiHelper) : BaseInteractor(apiHelper),
        SplashMVPInteractor {


    override fun checkSplashTimeout(): Observable<Long> {
        return Observable.timer(1, TimeUnit.SECONDS, Schedulers.io());
    }

}