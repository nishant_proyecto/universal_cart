Changed below DB information for connecting new database
File : api/models/LocalProduct.js

host = '127.0.0.1';
db_name = 'product_tracker';
user = 'root';
password = 'Test@123';



Rest API for search 
Type GET

http://localhost:8080/search/watch


Response :
Type JSON

[
    {
        "Id": "B078PB9XVJ",
        "href": "/gp/slredirect/picassoRedirect.html/ref=pa_sp_atf_aps_sr_pg1_1?ie=UTF8&adId=A08499771WARIUOMFT8IW&url=https%3A%2F%2Fwww.amazon.com%2FBETFEEDO-Ultra-Thin-Quartz-Analog-Leather%2Fdp%2FB078PB9XVJ%2Fref%3Dsr_1_1_sspa%2F143-5898284-1296444%3Fie%3DUTF8%26qid%3D1527056896%26sr%3D8-1-spons%26keywords%3Dwatch%26psc%3D1&qualifier=1527056895&id=8018829478661621&widgetName=sp_atf",
        "src": "https://images-na.ssl-images-amazon.com/images/I/51qb7PFCgdL._AC_US200_.jpg",
        "width": "200",
        "height": "200",
        "desc": "BETFEEDO Men's Ultra-Thin Quartz Analog Date Wrist Watch with Black Leather Strap (WHITE/RG)",
        "price": "$48.99"
    },
    {
        "Id": "B076P83XK1",
        "href": "/gp/slredirect/picassoRedirect.html/ref=pa_sp_atf_aps_sr_pg1_2?ie=UTF8&adId=A0019749ZO5FO7NLFYG9&url=https%3A%2F%2Fwww.amazon.com%2FWatches-Business-Fashion-Stainless-Waterproof%2Fdp%2FB076P83XK1%2Fref%3Dsr_1_2_sspa%2F143-5898284-1296444%3Fie%3DUTF8%26qid%3D1527056896%26sr%3D8-2-spons%26keywords%3Dwatch%26psc%3D1%26smid%3DA15GIKMUKYERDL&qualifier=1527056895&id=8018829478661621&widgetName=sp_atf",
        "src": "https://images-na.ssl-images-amazon.com/images/I/51lVGwTVEPL._AC_US200_.jpg",
        "width": "200",
        "height": "200",
        "desc": "Watches,Mens Watches,Luxury Business Fashion Stainless Steel Waterproof Analog Quartz Watch (Black)",
        "price": "$29.99"
    }
]



Rest API for product tracking
Type Post:
http://localhost:8080/start_tracker

Header Property:
Content-Type = 'application/json'
Request Body:
{
        "id": "B078PB9XVJ",
        "gcm_id": "abcdeff",
        "price": "48.99",
        "company":"Amazon"
}


Response:
{
    "state": "IDLE",
    "id": "B078PB9XVJ",
    "gcm_id": "abcdeff",
    "price": "48.99",
    "company": "Amazon",
    "updatedAt": "2018-05-23T06:37:42.390Z",
    "createdAt": "2018-05-23T06:37:42.390Z"
}