"use strict";
const https = require('https');
var request = require('request');
var cheerio = require('cheerio');
var _ = require('underscore');
var htmlparser = require("htmlparser2");
var LocalProduct = require('../models/Product');
var PriceTracker = require('../models/PriceTracker');

//var amzn_url = 'https://www.amazon.com/s/ref=nb_sb_noss_2?url=search-alias%3Daps&field-keywords='
  var amzn_url =  'https://www.amazon.com/s/ref=nb_sb_noss_1?url=search-alias%3Daps&field-keywords='



exports.getProducts = function(req, res){

    LocalProduct.getAllProducts(function(products, err){
        if(err){
            console.log(err);
            res.statusCode = 403;
            res.json(err);
        }else{     
            res.json(products);
        }
    });

}

exports.deleteProduct = function(req, res){

    var productId = req.params.productId;
    LocalProduct.deleteProduct(productId,function(products, err){
            if(err){
                console.log(err);
                res.statusCode = 403;
                res.json(err);
               }else{     
                    res.json(products);
               }
    });

}


exports.deleteProductByUserId = function(req, res){

    var userId = req.params.userId;
    LocalProduct.deleteProductByUserId(userId,function(products, err){
            if(err){
                console.log(err);
                res.statusCode = 403;
                res.json(err);
               }else{     
                    res.json(products);
               }
    });

}


exports.getProductsByUserId = function(req, res){

    var userId = req.params.userId;
    LocalProduct.getProductsUserId(userId,function(products, err){
            if(err){
                console.log(err);
                res.statusCode = 403;
                res.json(err);
               }else{     
                    res.json(products);
               }
    });

}


exports.startTrackerForProduct = function (req, res){
    
    var product = req.body;
    console.log("Save Product :"+product);
    LocalProduct.saveProductTracker(product,function(err,insertedProduct){
       // var result = {isError:false,errorCode:null,errorDescription:null,product:JSON.stringify(insertedProduct)};
       if(err){
        console.log(err);
        res.statusCode = 403;
        res.json(err);
       }else{ 
        console.log(insertedProduct);
        res.json(insertedProduct);
        PriceTracker.startTrackPrice(insertedProduct);
    }
    });
}


exports.searchProduct = function(req, res){
     
        
        var prodcuts = [];

        var keyword = req.params.keyword;
    
        console.log("search keyword: "+keyword);

        var urlWithParam  = amzn_url+ keyword;

        var options = {
            url: urlWithParam,
            headers: {
              'User-Agent': 'request',
              'Accept':'text/html,application/xhtml+xml,application/xml',
              'Accept-Language':'it-IT,it'
            }
          };
          

          function callback(error, response, body) {
            if (!error && response.statusCode == 200) {
              
           
            

            var $ = cheerio.load(body);
            
            var isProductFound = false;
            var currency;
            var fractionPrice;
            var wholePrice;
            var product;
            $('*').each(function (index, element) { 
                
                
                var id = $(element).attr('data-asin');
                if(id){
                    product = {};
                    product.id = id;
                    isProductFound = true;
                }

              
            if(isProductFound){
               var href = $(element).attr('href');
               if(href){
                console.log('href :'+href);
                product.href = href;
               }



                var src = $(element).attr('src');
               
                if(src){
                    console.log('src :'+src);
                    product.src = src;
                }
               
                var width = $(element).attr('width');

                
                if(width){
                    console.log('width :'+width);
                    product.width = width;     
                }
                var height = $(element).attr('height');
           

                if(height){
                    console.log('height :'+height);
                    product.height = height; 
                }

                
                var alt = $(element).attr('alt');

                if(alt){
                    console.log('alt :'+alt);
                    product.description= alt;
                 
                }


                var sx_price_currency_tag =  $(element).attr('class');
                if(sx_price_currency_tag == 'sx-price-currency'){
                   var sx_price_currency = element.children[0].nodeValue;
                    currency =   sx_price_currency;            
                }
                
                var sx_price_whole_tag =  $(element).attr('class');
                if(sx_price_whole_tag == 'sx-price-whole'){
                   var sx_price_whole = element.children[0].nodeValue;
                   wholePrice = sx_price_whole;                   
                }

                
                var sx_price_fractional_tag =  $(element).attr('class');
                if(sx_price_fractional_tag == 'sx-price-fractional'){
                   var sx_price_fractional = element.children[0].nodeValue;
                   fractionPrice = sx_price_fractional;

                }

                if(currency && wholePrice && fractionPrice){
                    var price = currency+wholePrice+'.'+fractionPrice;             
                   product.price = price;
                   prodcuts.push(product);
                   product = undefined;
                   isProductFound = false;
                   wholePrice = undefined;
                   currency = undefined;
                   fractionPrice = undefined;
       
                }

            }

            
        });
           

           console.log("Totaal product :"+prodcuts.length);
           if(prodcuts.length == 0){
               var status = {};
               status.isError = true;
               status.errorCode = 'PRODUCT_NOT_FOUND';
               status.errorDescription = 'Product not found';
               res.json(status);
               return;
           }
           
         }
            console.log(prodcuts);
            res.json(prodcuts);
            
        }
          
          request(options, callback);
     
    }

    
    



    exports.registerGcm = function(req,res){

    var gcmId = req.params.gcmId;
    console.log("GCM ID: "+gcmId);
    
    var res ={};
    res.isError = false;
    res.errorCode = null;
    res.errorDescription =null;
    res.json(res);
}




