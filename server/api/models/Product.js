// const host = '127.0.0.1';
// const db_name = 'product_tracker';
// const user = 'root';
// const password = 'Test@123';
var Product = require('../db/DB').Product;




exports.saveProductTracker = function(product,callback){

    // force: true will drop the table if it already exists
   Product.sync().then(() => {
    // Table created
      Product.create(product).then((result)=>{
        var temp = result.get({plain: true});
        
        console.log(temp); // =
        callback(undefined,temp);
        
      }).catch(err => {
        console.error('database err:', err);
        callback(err,undefined);
      });
      
    }).catch(err => {
      console.error('Unable to connect to the database:', err);
      callback(err,undefined);
    });

}


exports.getAllProducts = function(callback){
     Product.findAll().then(function(results){
            callback(results,undefined);
     }).catch(err => {
      console.error('Unable to connect to the database:', err);
      callback(undefined,err);
    });
     
}

exports.getProduct = function(productId,callback){
    Product.findById(productId)
  .then(function(results) {
        if(results == null){
          callback(undefined,'product is not present');
        }else{
        callback(results,undefined);
      }
      }).catch(err => {
    console.error('Unable to connect to the database:', err);
    callback(undefined,err);
  });;
}


exports.getProductsUserId = function(userId,callback){
  Product.findAll({user_id:userId})
.then(function(results) {
      callback(results,undefined);
}).catch(err => {
  console.error('database err:', err);
  callback(undefined,err);
});
}




exports.updateProduct = function(id, state){
  Product.update(
    { state: state },
    { where: { id: id } }
  )
    .then(result =>
      console.log(result)
      ///handleResult(result)
    )
    .then(err =>
      console.log("err:"+err)
    )
}

exports.deleteProduct = function(id,callback){
  Product.destroy({where:{id:id}})
    .then(function(results) {
      callback(results,undefined);
    }).catch(err => {
        console.error('database err:', err);
        callback(undefined,err);
    });
}

exports.deleteProductByUserId = function(userId,callback){
  Product.destroy({where:{user_id:userId}})
    .then(function(results) {
      callback(results,undefined);
    }).catch(err => {
        console.error('database err:', err);
        callback(undefined,err);
    });
}




exports.isExist = function(id,callback){
  Product.isExist
  
  Product.afterDelete({where:{id:id}})
    .then(function(results) {
      callback(results,undefined);
    }).catch(err => {
        console.error('database err:', err);
        callback(undefined,err);
    });
}
