var request = require('request');
//var pb = require('pushbullet');
var cheerio = require('cheerio');
var _ = require('underscore');
var GCMSender = require('./GCMSender');
var LocalProduct = require('./Product');
// var lodash= require('lodash');

// Variables
class PriceTracker{

    constructor(product){
        this.product = product;
        //this.pb_token = "o.pgjEG7Vy4GT5ta449GL4TIXhCWSWHtOi";
        this.amzn_url = 'http://www.amazon.com/dp/'+product.id;
        this.span_id = '#data-asin-price';
        this.check_interval = 60000;
        if(product.price.startsWith('$')){
            this.price = product.price.substring(1,product.price.length);    
        }else{
            this.price = product.price;
        }
            
    }

    startPriceTracking(){
        this.product.state = 'TRACKING';
        LocalProduct.updateProduct(this.product.id,this.product.state);
        setTimeout(checkPrice, 0,this);
    }
 
}

function checkPrice(tracker){

    
    LocalProduct.getProduct(tracker.product.id,function(product,err){
        if(!err){
            downloadHtml(tracker);
        }else{
            console.log("product is removed: "+err);
        }
    });
    console.log("Check price: "+tracker.amzn_url);
}



function downloadHtml(tracker){
    request(tracker.amzn_url, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            var $ = cheerio.load(body);

            $('div[id="cerberus-data-metrics"]').each(function (index, element) {
                console.log("Product Name - "+$(element).data('asin')+" : $"+$(element).data('asin-price'));
                var list_price =$(element).data('asin-price');
                
                if (Number(list_price) >= Number(tracker.price)) {
                    tracker.price = list_price;
                    sendPush(tracker);
                }else{
                    console.log("Wait for best price...");	
                }
            });


        }
        else {
            console.log("Uh oh. There was an error.");
        }


        setTimeout(checkPrice, tracker.check_interval,tracker);

});

}





exports.startTrackPrice = function(produt){

    var tracker = new PriceTracker(produt);
    tracker.startPriceTracking();    

}

function sendPush(tracker) {
    GCMSender.sendGCMMessage(tracker.product,tracker.price);

}

const getAllAttributes = function (node) {
	return node.attributes || _.map(node.attribs,(value,name) => {
		return { name,value };
	});
};

