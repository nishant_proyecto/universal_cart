
//var sequelize = require('../db/DB');
var User = require('../db/DB').User;



 

exports.saveUser = function(user,callback){

    // force: true will drop the table if it already exists
    User.sync().then(() => {
    // Table created
      User.create(user).then((result)=>{
        var temp = result.get({plain: true});
        
        console.log(temp); // =
        callback(undefined,temp);
        
      }).catch(err => {
        console.error('database err:', err);
        callback(err,undefined);
      });
      
    }).catch(err => {
      console.error('Unable to connect to the database:', err);
      callback(err,undefined);
    });

}


exports.getAllUser = function(callback){
     User.findAll().then(function(results){
            callback(results,undefined);
     }).catch(err => {
      console.error('Unable to connect to the database:', err);
      callback(undefined,err);
    });
     
}

exports.getUser = function(userId,callback){
    User.findById(userId)
  .then(function(results) {
        if(results == null){
          callback(undefined,'product is not present');
        }else{
        callback(results,undefined);
      }
      }).catch(err => {
    console.error('Unable to connect to the database:', err);
    callback(undefined,err);
  });;
}





exports.updateUser = function(user,callback){
  User.update(
    { gcm_id: user.gcm_id },
    { where: { id: user.id } }
  ).then(function(result){
      console.log(result)
      callback(result,undefined);
  }).then(function(err){
      console.log("err:"+err)
        callback(undefined,err)
    });
}

exports.deleteUser = function(id,callback){
  User.destroy({where:{id:id}})
    .then(function(results) {
      callback(results,undefined);
    }).catch(err => {
        console.error('database err:', err);
        callback(undefined,err);
    });
}
