const host = 'www.db4free.net';
const db_name = 'test_product';
const user = 'test_product';
const password = 'Test@1234';

const Sequelize = require('sequelize');
const sequelize = new Sequelize(db_name, user, password, { 

insecureAuth : true,
  host: host,
  dialect: 'mysql',
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },

  // SQLite only
 // storage: 'path/to/database.sqlite'
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

  var Product = sequelize.define('product', {
    id: {type:Sequelize.STRING, primaryKey: true},
    description: {type:Sequelize.TEXT('long')},
    href:{type:Sequelize.TEXT('long')},
    src:{type:Sequelize.TEXT('long')},
    height:{type:Sequelize.STRING},
    width:{type:Sequelize.STRING},
    user_id: {type:Sequelize.TEXT('long'),allowNull: false},
    state: {type:Sequelize.STRING,defaultValue:'IDLE'},
    price:Sequelize.STRING,
    company:{type:Sequelize.STRING,allowNull: false}
  });


var User = sequelize.define('user', {
    id: {type:Sequelize.STRING, primaryKey: true},
    gcm_id: {type:Sequelize.TEXT('long'),allowNull: false},
  });



exports.Product = Product;
exports.User = User;