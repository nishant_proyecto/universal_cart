'use strict';
module.exports = function(app) {
  var ProductController = require('../controllers/ProductController');

  var  UserController = require('../controllers/UserController');
  

  // todoList Routes
  app.route('/search/:keyword')
    .get(ProductController.searchProduct);


    
  app.route('/product')
  .get(ProductController.getProducts);

  app.route('/product/:productId')
  .get(ProductController.getProducts).delete(ProductController.deleteProduct);

  app.route('/product/user/:userId')
  .get(ProductController.getProductsByUserId).delete(ProductController.deleteProductByUserId);


  app.route('/start_tracker')
    .post(ProductController.startTrackerForProduct);

  app.route('/user')
    .post(UserController.register).delete(UserController.delete).put(UserController.update);
    
};
